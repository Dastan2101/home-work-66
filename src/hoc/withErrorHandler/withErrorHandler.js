import React, {Component, Fragment} from 'react';
import Modal from "../../components/UI/modal/Modal";

const withErrorHandler = (WrappedComponent, axios) => {

  return class WithErrorHoc extends Component{
      constructor(props) {
          super(props);
          this.state = {
              error: null,
              loading: false
          };

          this.state.interseptorReq = axios.interceptors.request.use(request =>  {
              this.setState({loading: true});
              return request
          }, error => {
              this.setState({error, loading: false});
              throw error
          });

          this.state.interseptorRes = axios.interceptors.response.use(response => {
              this.setState({loading: false});

              return response
          }, error => {
              this.setState({error, loading: false});
              throw error
          });

      }

      componentWillUnmount() {
          axios.interceptors.request.eject(this.state.interseptorReq);
          axios.interceptors.response.eject(this.state.interseptorRes);
      }


      errorDismissed = () => {
          this.setState({error: null})
      };

      render() {
          return (
              <Fragment>
                  {this.state.loading && <Modal show={this.state.error} close={this.errorDismissed}>
                      {this.state.error && this.state.error.message}
                  </Modal>}
                  <WrappedComponent {...this.props}/>
              </Fragment>
          )
      }

  }
};

export default withErrorHandler;