import React, {Component} from 'react';
import axios from '../../axios-quotes';
import withErrorHandler from "../../hoc/withErrorHandler/withErrorHandler";

class GetQuotes extends Component {

    state = {
      quotes: null,
    };

    componentDidMount() {
        axios.get('/quotes.json').then(response => {
            const quotes = Object.keys(response.data).map(id => {
                return {...response.data[id], id}

            });
            this.setState({quotes})
        })
    }

    render() {
        if (!this.state.quotes) {
            return null
        }
        return (

        this.state.quotes.map(quote => {
            return (
                <div key={quote.id} style={{border: '1px solid grey', padding: '20px', margin: '5px'}}>
                    <h5>{quote.category}</h5>
                    <h6>{quote.quote}</h6>
                    <h6>{quote.author}</h6>
                </div>
                )

        })
        );

    }
}

export default withErrorHandler(GetQuotes, axios);