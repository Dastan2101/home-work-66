import React from 'react';
import './Backdrop.css';
import Spinner from "../Spinner/Spinner";

const Backdrop = props => (
    props.show ? <div className="Backdrop" onClick={props.onClick} /> : <Spinner/>
);

export default Backdrop;