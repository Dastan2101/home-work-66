import React from 'react';
import NavigationItem from "./NavigationItem/NavigationItem";
import './NavigationItems.css';

const NavigationItems = () => (
    <ul className="NavigationItems">
        <NavigationItem to="/" exact>Home</NavigationItem>
        <NavigationItem to="/get" exact>Get quotes</NavigationItem>
    </ul>
);

export default NavigationItems;