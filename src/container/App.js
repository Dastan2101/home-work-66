import React, { Component } from 'react';
import './App.css';
import Layout from "../components/Layout/Layout";
import {Switch, Route} from "react-router-dom";
import GetQuotes from "../components/GetQuotes/GetQuotes";

class App extends Component {
  render() {
    return (
        <Layout>
            <Switch>
                <Route path="/get" component={GetQuotes}/>
                <Route path="/" render={() => <h1 style={{textAlign: 'center'}}>Home page</h1>}/>
            </Switch>

        </Layout>
    );
  }
}

export default App;
